package io.github.coderoly.campusshoutout.slideshow;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.WindowManager;

import io.github.coderoly.campusshoutout.R;

public class SlideShow extends Fragment {
/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideshow_fragment);

        ViewPager pager = (ViewPager) findViewById(R.id.container);
     //   pager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
    }
*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.slideshow_fragment, container, false);

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        ViewPager pager = (ViewPager) v.findViewById(R.id.container);
        //   pager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        pager.setAdapter(new MyPagerAdapter(getFragmentManager()));

        return v;
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                    return FirstFragment.newInstance("Slideshow 1");
                case 1:
                    return ThirdFragment.newInstance("Slideshow 2");
                case 2:
                    return SecondFragment.newInstance("Slideshow 3");
                case 3:
                    return ThirdFragment.newInstance("Slideshow 4");
                case 4:
                    return LastFragment.newInstance(null);
                default:
                    return LastFragment.newInstance(null);
            }
        }

        @Override
        public int getCount() {
            return 5;
        }
    }


}

package io.github.coderoly.campusshoutout.authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;

import io.github.coderoly.campusshoutout.R;

/**
 * Created by oly on 11/05/17.
 */

public class Signup extends Fragment implements View.OnClickListener {

    Button authBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_signup, container, false);


        // getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        v.findViewById(R.id.SignupBtnBack).setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.SignupBtnBack:
                getFragmentManager().beginTransaction().replace(R.id.MainActivityContainer, new AuthChooser()).commit();
                break;
        }
    }


    private void StartLoginProcess(View v) {

        EditText email = (EditText) v.findViewById(R.id.LoginEtEmail);
        //String typedEmail = email.getText().toString();
        final String typedEmail = "oliver.mwitia@gmail.com";
        EditText password = (EditText) v.findViewById(R.id.LoginEtPassword);
        //String typedPassword = password.getText().toString();
        final String typedPassword = "oliver.mwitia@gmail.com";


        FirebaseAuth.getInstance().fetchProvidersForEmail(typedEmail)
                .addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                    @SuppressWarnings("ConstantConditions")
                    @Override
                    public void onComplete(@NonNull Task<ProviderQueryResult> task) {

                        /* getProviders() will return size 1. if email ID is available.*/
                        if (task.isSuccessful()) {

                            //TODO  dialogMessages.Hide_Progress_Dialog();

                            String doesEmailExist = "";


                            if (task.getResult().getProviders().size() == 1)
                                doesEmailExist = "yes";
                            else if (task.getResult().getProviders().size() == 0)
                                doesEmailExist = "no";
                            else {
                                Toast.makeText(getActivity(), "An Error Occurred", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            switch (doesEmailExist) {
                                case "no":
                                    new FirebaseAuthMethods().RegisterUser(typedEmail, typedPassword, getActivity());
                                    return;
                                case "yes":
                                    new FirebaseAuthMethods().LogInUser(typedEmail, typedPassword, getActivity());
                                    return;
                                default:
                                    //TODO Error (An Error Occurred)
                                    Toast.makeText(getActivity(), "An Error Occurred", Toast.LENGTH_SHORT).show();
                                    break;
                            }

                        } else {
                            //TODO Error (An Error Occurred)
                            Toast.makeText(getActivity(), "An Error Occurred", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


    }


}

